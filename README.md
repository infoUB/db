Reposotori de l'assignatura de Bases de Dades

### TO-DO
- [ ] !!! S'ha de sobreescriure el diagrama de covid per la versió que s'ha fet servir !!!
- [ ] Repassar cardinalitats de les relacions
- [ ] Posar les entitats i relacions febles

### Altres Comentaris 
## Covid
- Efectes secundaris hauria de ser atribut amb atributs (veure diapositiva 39 de BD-T5)

## Rover
- idRegistre hauria de ser primary key de Meteo? 
- Una mostra pot tenir més d'una imatge. Podem fer una taula que relacioni mostra i imatge, o posar un atribut a imatge per relacionar-la amb la mostra i que pugui ser NULL...? Prefereixo la primera, que així no embrutem Imatge
