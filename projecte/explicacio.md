## Explicació del diagrama

Cada tren està modelitzat a l'entitat **tren**, i està identificat amb 
una cadena alfanumèrica única. Els trens poden ser de dos tipus: de
passatgers o de mercaderies. En el cas de trens de mercaderies es
guardarà el nombre de vagons que transporten i la referència del
manifest de les mercaderies. En el cas de transport de passatgers es
guardarà el nombre d'unitats que el componen i el nombre de cotxes que
sumen les diverses unitats.

Els trens circulen per la xarxa ferroviària i paren a les **estacions**.
L'entitat **estació** guardarà un identificador únic, la seva adreça, el
nombre de vies que té, informació sobre l'accessibilitat i la zona
tarifària on es troba. Les estacions s'agrupen per **línies**, que no
són més que recorreguts de successions d'estacions. Cada línia tindrà un
identificador únic i guardarà una referència a l'estació inicial i final
de la línia. Una estació pot formar part de diverses línies.

Per controlar la circulació dels trens farem servir l'entitat
**viatge**. Un viatge no és res més que una circulació completa sobre
una línia. Cada viatge serà efectuat per un únic maquinista, de manera
que es guardarà l'identificador d'aquest. A més, com que una línia pot
ser recorreguda en dos sentits, es guardarà la referència a l'estació de
final de trajecte. Cada trajecte seguirà un horari, de manera que tindrà
assignades una o més **parades**. Cada parada es farà en una **estació**
i tindrà una hora d'arribada i una de sortida (amb, possiblement,
l'excepció de l'inici i final de línia).

Els passatgers poden comprar un billet que els permet fer un **viatge**,
que forma part d'un o més trajectes. Es guardarà l'estació inicial i
final del viatge i es calcularà el preu en funció del nombre de zones
per on es passi (el preu serà una funció del valor absolut de la
diferència entre la zona de l'estació inicial i final més un).

## Possibles ampliacions
Es pot introduir:

 - Una entitat persona que pot especialitzar en Viatger, Maquinista,
    Serveis...

-   En lloc de persona, separar directament entre client (que pot
    especialitzar en particular o mercaderies) i personal (maquinistes,
    manteniment, neteja, personal d'estació)

-   Una entitat billet amb un sistema de tarifes més complex

-   Ara mateix l'entitat tren fa referència a el tren sencer, podem
    separar tren en entitats més petites (locomotores, vagons [i
    especialitzar vagons si cal], unitats autopropulsades [i
    especialitzar si cal]...)

-   Un sistema més complex de mercaderies, ara només guardem una id de
    manifest que no fa referència a res, podem fer l'entitat manifest o
    afegir alguna cosa més en aquest sentit

-   Separar d'alguna manera els trens de passatgers en rodalies,
    regional, mitja distància, alta velocitat...

## Coses que no tinc clares

-   Com fem la relació entre viatge i trajecte?

-   Com gestionem el tema dels horaris? Ara mateix tindríem una taula
    gegant on tenim una entrada per cada tren que pari a una estació.