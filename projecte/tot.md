\maketitle
\pagenumbering{gobble}
\thispagestyle{plain}
La nostra proposta consisteix en la modelació d'un sistema de transport
públic. Com a usuaris habituals, sovint hem de fer consultes referents a
aquest sistema i l'experiència no sempre és bona. Per posar un exemple,
sovint havíem tingut problemes amb l'anterior disseny de la pàgina de
consulta d'horaris de la RENFE[^1] i creiem que es podrien donar moltes
més dades de les que s'ofereixen en fer aquestes consultes. Per això hem
pensat en modelar una xarxa d'aquestes característiques i aplicar el
model a una xarxa real (ja sigui Rodalies de Catalunya, la xarxa de
Ferrocarrils de la Generalitat o bé el metro de Barcelona) i fer
nosaltres mateixos l'explotació d'aquestes dades. A més, totes aquestes
operadores ofereixen conjunts de dades bastant complets que ens poden
facilitar molt la recaptació de dades reals.

Pensem que aquesta base de dades pot ser útil a molts nivells: la podrà
utilitzar des del centre de control per saber la posició de cada unitat,
un aficionat per saber el número de sèrie de la unitat que passa per
Blanes a les 13:42 o una persona que viatja amb cadira de rodes per
saber si les estacions són accessibles o no.

A més, es tracta d'un sistema complex amb molts elements (vehicles,
estacions, línies, horaris, tarifes\...), de manera que creiem que es un
projecte amb suficient material per tal de tractar diferents aspectes de
la assignatura.

A continuació presentem la primera versió de l'esquema entitat-relació:

Explicació del diagrama {#explicació-del-diagrama .unnumbered .unnumbered}
-----------------------

Cada tren està modelitzat a l'entitat **tren**, i està identificat amb
una cadena alfanumèrica única. Els trens poden ser de dos tipus: de
passatgers o de mercaderies. En el cas de trens de mercaderies es
guardarà el nombre de vagons que transporten i la referència del
manifest de les mercaderies. En el cas de transport de passatgers es
guardarà el nombre d'unitats que el componen i el nombre de cotxes que
sumen les diverses unitats.

Els trens circulen per la xarxa ferroviària i paren a les **estacions**.
L'entitat **estació** guardarà un identificador únic, la seva adreça, el
nombre de vies que té, informació sobre l'accessibilitat i la zona
tarifària on es troba. Les estacions s'agrupen per **línies**, que no
són més que recorreguts de successions d'estacions. Cada línia tindrà un
identificador únic i guardarà una referència a l'estació inicial i final
de la línia. Una estació pot formar part de diverses línies.

Per controlar la circulació dels trens farem servir l'entitat
**viatge**. Un viatge no és res més que una circulació completa sobre
una línia. Cada viatge serà efectuat per un únic maquinista, de manera
que es guardarà l'identificador d'aquest. A més, com que una línia pot
ser recorreguda en dos sentits, es guardarà la referència a l'estació de
final de trajecte. Cada trajecte seguirà un horari, de manera que tindrà
assignades una o més **parades**. Cada parada es farà en una **estació**
i tindrà una hora d'arribada i una de sortida (amb, possiblement,
l'excepció de l'inici i final de línia).

Els passatgers poden comprar un billet que els permet fer un **viatge**,
que forma part d'un o més trajectes. Es guardarà l'estació inicial i
final del viatge i es calcularà el preu en funció del nombre de zones
per on es passi (el preu serà una funció del valor absolut de la
diferència entre la zona de l'estació inicial i final més un).

Possibles ampliacions {#possibles-ampliacions .unnumbered .unnumbered}
---------------------

Es pot introduir:

-   Una entitat persona que pot especialitzar en Viatger, Maquinista,
    Serveis\...

-   En lloc de persona, separar directament entre client (que pot
    especialitzar en particular o mercaderies) i personal (maquinistes,
    manteniment, neteja, personal d'estació)

-   Una entitat billet amb un sistema de tarifes més complex

-   Ara mateix l'entitat tren fa referència a el tren sencer, podem
    separar tren en entitats més petites (locomotores, vagons \[i
    especialitzar vagons si cal\], unitats autopropulsades \[i
    especialitzar si cal\]\...)

-   Un sistema més complex de mercaderies, ara només guardem una id de
    manifest que no fa referència a res, podem fer l'entitat manifest o
    afegir alguna cosa més en aquest sentit

-   Separar d'alguna manera els trens de passatgers en rodalies,
    regional, mitja distància, alta velocitat\...

Coses que no tinc clares {#coses-que-no-tinc-clares .unnumbered .unnumbered}
------------------------

-   Com fem la relació entre viatge i trajecte?

-   Com gestionem el tema dels horaris? Ara mateix tindríem una taula
    gegant on tenim una entrada per cada tren que pari a una estació.

[^1]: <https://www.renfe.com/es/es/cercanias/rodalies-catalunya/horarios>
