/*
SELECT owner_id, SUM(rent) AS Total_rent
FROM property4rent
GROUP BY owner_id;

SELECT branch_id, ROUND(SUM(salary),2) AS Salary_add, COUNT(staff_id) AS Total_employees
FROM staff 
GROUP BY branch_id;


SELECT 
    branch_id,
    GROUP_CONCAT(' \'', lname, '\'') AS Employees_last_name,
    MIN(salary) AS Min_salary
FROM staff
GROUP BY branch_id
HAVING Min_salary >= 10000;


SELECT 
    owner_id,
    SUM(rent) AS Total_rent,
    GROUP_CONCAT(DISTINCT(city) SEPARATOR ', ') AS Cities
FROM property4rent
GROUP BY owner_id
HAVING Total_rent > 400
ORDER BY owner_id DESC;

SELECT *
FROM viewing
WHERE property_id IN (SELECT property_id FROM property4rent WHERE city != 'Glasgow');


SELECT *
FROM property4rent
WHERE staff_id IN (
	SELECT staff_id
	FROM staff
	WHERE branch_id IN (
		SELECT branch_id
		FROM branch
		WHERE city = 'Glasgow'));
        
SELECT fname, lname, position, salary - (SELECT AVG(salary) FROM staff) AS Diff_AVG_Salary
FROM staff
WHERE salary > (SELECT AVG(salary) FROM staff);


SELECT *
FROM staff
WHERE salary >= ANY(
	SELECT salary
	FROM staff
	WHERE branch_id IN(
		SELECT branch_id
		FROM branch
		WHERE city = 'Glasgow'));
        
SELECT *
FROM staff
WHERE salary > ALL(
	SELECT salary
	FROM staff
	WHERE branch_id IN(
		SELECT branch_id
		FROM branch
		WHERE city = 'Glasgow'));
        
START TRANSACTION;
DELETE FROM branch WHERE branch_id = 'B005'
;
SELECT * FROM branch;
ROLLBACK;


SELECT * FROM branch;


CREATE VIEW personal AS
SELECT staff_id, position, salary, branch_id
FROM staff;

SELECT * FROM personal;

DROP VIEW comp_staff;
*/


SELECT * FROM property4rent;




