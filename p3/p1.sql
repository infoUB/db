/*
SELECT * FROM staff;
SELECT staff_id, fname, lname, salary FROM staff;
SELECT staff_id, salary AS salari_mensual FROM staff;
SELECT DISTINCT property_id	FROM viewing;
SELECT * FROM staff WHERE salary > 10000;
SELECT street, city FROM branch WHERE city = 'London' OR city = 'Glasgow';
SELECT fname, lname, position, salary FROM staff WHERE salary >= 10000 AND salary <= 30000;
SELECT fname, lname, position, salary FROM staff WHERE salary BETWEEN 10000 AND 30000;
SELECT fname, lname, position FROM staff WHERE position IN ('Manager','Supervisor');
SELECT * FROM privateowner WHERE address LIKE '%Glasgow%';
SELECT client_id, property_id, viewdate FROM viewing WHERE comment IS NULL;
SELECT * FROM staff ORDER BY salary DESC;
SELECT * FROM property4rent ORDER BY type;
SELECT * FROM property4rent ORDER BY type, rent;
SELECT COUNT(*) AS Total FROM property4rent WHERE rent > 450;
SELECT COUNT(DISTINCT property_id) AS Total FROM viewing WHERE viewdate BETWEEN '2016-03-01' AND '2016-04-30';
SELECT COUNT(staff_id) AS total_managers, SUM(salary) AS suma_salaris FROM staff WHERE position = 'Manager';
SELECT MIN(salary) AS minim_salari, MAX(salary) AS maxim_salari, AVG(salary) AS mitja_salari FROM staff;
INSERT INTO privateowner
(privateowner_id, fname, lname, address, telnumber)
VALUES
('CO99', 'Jack', 'Dew', '4 Fergus Dr, Aberdeen AB2 7SX', 1411411411);

SELECT * FROM privateowner;

INSERT INTO property4rent
(property_id, staff_id, branch_id, owner_id, street, city, postcode, type, rooms, rent)
VALUES
('PA15','SA9','B007','CO99','12 Holhead', 'Aberdeen', 'AB7 5SU', 'House', 6, 600),
('PA16','SA9','B007','CO99','14 Holhead', 'Aberdeen', 'AB7 5SU', 'House', 6, 600);
*/

SELECT * FROM property4rent WHERE city = 'Aberdeen';


