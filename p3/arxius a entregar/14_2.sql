/*
-- Query: SELECT * FROM property4rent
LIMIT 0, 1000

-- Date: 2021-05-03 01:25
*/
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PA14','16 Holhead','Aberdeen','AB7 5SU','House',6,650,'CO46','SA9','B007');
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PG16','5 Novar Dr','Glasgow','G12 9AX','Flat',4,450,'CO93','SG14','B003');
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PG21','18 Dale Rd','Glasgow','G12','House',5,600,'CO87','SG37','B003');
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PG36','2 Manor Rd','Glasgow','G32 4QX','Flat',3,375,'CO93','SG37','B003');
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PG4','6 Lawrence St','Glasgow','G11 9QX','Flat',3,350,'CO40',NULL,'B003');
INSERT INTO `` (`property_id`,`street`,`city`,`postcode`,`type`,`rooms`,`rent`,`owner_id`,`staff_id`,`branch_id`) VALUES ('PL94','6 Argyll St','London','NW2','Flat',4,400,'CO87','SL41','B005');
